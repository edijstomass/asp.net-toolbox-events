﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsExamples
{
    public partial class IsPostBackEx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Response.Write("Page loaded for the first time");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}