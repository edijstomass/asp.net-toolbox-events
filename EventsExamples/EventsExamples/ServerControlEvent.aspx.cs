﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsExamples
{
    public partial class ServerControlEvent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write("Button Clicked" + "<br/>"); // Sis ir PostBackEvent - immediately process with server
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // cached events - are saved in view state, waiting for post back event occurs
            // cached events can be transformed to PostBackEvent - go to textbox1 propery and change
            //Auto Post Back to TRUE
            Response.Write("Text Changed" + "<br/>"); 
        }
    }
}