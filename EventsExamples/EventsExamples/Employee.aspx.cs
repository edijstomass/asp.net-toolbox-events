﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsExamples
{
    public partial class Employee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCityDropDownList();
            }

        }

        public void LoadCityDropDownList()
        {
            ListItem li1 = new ListItem("London");
            ListItem li2 = new ListItem("Sidney");
            ListItem li3 = new ListItem("Mumbai");

            ddlCity.Items.Add(li1);
            ddlCity.Items.Add(li2);
            ddlCity.Items.Add(li3);
            ddlCity.Items.Add("Kuldiga");

        }




    }
}