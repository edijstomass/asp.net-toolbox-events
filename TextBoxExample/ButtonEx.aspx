﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ButtonEx.aspx.cs" Inherits="TextBoxExample.ButtonEx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="Button1"
                        runat="server"
                        OnClick="Button1_Click"
                        Text="Submit" />
            <br />
            <br />
            <br />
            <br />

            <asp:LinkButton ID="LinkButton1" 
                            runat="server" 
                            OnClientClick="return confirm('You are about to submit this page')"
                            OnClick="LinkButton1_Click1">Submit</asp:LinkButton>

            <br />
            <br />
            <br />
            <asp:ImageButton ID="ImageButton1" 
                             runat="server" 
                             ImageUrl="~/Images/images.jpg" 
                             OnClick="ImageButton1_Click" 
                             OnClientClick="alert('You are about to submit this page')" />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
