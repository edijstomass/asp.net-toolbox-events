﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TextBoxExample
{
    public partial class RadioButtonEx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(MaleRadioButton.Checked)
                {
                Response.Write("Your gender is: "+ MaleRadioButton.Text + "</br>");
            }
            else if (FemaleRadioButton.Checked)
            {
                Response.Write("Your gender is: " + FemaleRadioButton.Text + "</br>");
            }
            else
            {
                Response.Write("Your gender is: " + UnknownRadioButton.Text + "</br>");
            }
        }
    }
}