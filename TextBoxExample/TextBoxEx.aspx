﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TextBoxEx.aspx.cs" Inherits="TextBoxExample.TextBoxEx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 393px">
    <form id="form1" runat="server">
        <div style="height: 179px">
            <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True" Height="33px" OnTextChanged="TextBox1_TextChanged" ToolTip="Enter your name" Width="144px"></asp:TextBox>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Height="44px" OnClick="Button1_Click" Text="Button" Width="105px" />
        </div>
    </form>
</body>
</html>
