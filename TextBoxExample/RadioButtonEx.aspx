﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RadioButtonEx.aspx.cs" Inherits="TextBoxExample.RadioButtonEx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 159px">
            <fieldset style="width: 369px; height: 74px;">
                <legend><b>Gender</b></legend>
                

                <asp:RadioButton ID="MaleRadioButton" runat="server" GroupName="GenderGroup" Text="Male" />
                <asp:RadioButton ID="FemaleRadioButton" runat="server" GroupName="GenderGroup" Text="Female" />
                <asp:RadioButton ID="UnknownRadioButton" runat="server" GroupName="GenderGroup" Text="Unknown" />
                

            </fieldset>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </div>
    </form>
</body>
</html>
