﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HyperLinkEx.aspx.cs" Inherits="TextBoxExample.HyperLinkEx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.google.lv">Go to Google</asp:HyperLink>
            <br />
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" ImageUrl="~/Images/IMG.png" NavigateUrl="http://www.google.lv" Target="_blank">Go to Google</asp:HyperLink>
        </div>
    </form>
</body>
</html>
