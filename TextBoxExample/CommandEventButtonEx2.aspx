﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommandEventButtonEx2.aspx.cs" Inherits="TextBoxExample.CommandEventButtonEx2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="PrintButton" runat="server" CommandName="Print" OnCommand="CommandButton_Click" Text="Print" />
            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" OnCommand="CommandButton_Click" Text="Delete" />
            <asp:Button ID="Top10Button" runat="server" CommandArgument="Top10" CommandName="Show" OnCommand="CommandButton_Click" Text="Show TOP 10 Employees" />
            <asp:Button ID="Bottom10Button" runat="server" CommandArgument="Bottom10" CommandName="Show" OnCommand="CommandButton_Click" Text="Show Bottom top 10 employees" />
            <asp:Label ID="OutputLabel" runat="server" Text="Output Label"></asp:Label>
        </div>
    </form>
</body>
</html>
